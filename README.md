# Revents: Events app built with React, Redux and Firestore

This project contains code from studying [Build an app with React, Redux and Firestore from scratch](https://www.udemy.com/build-an-app-with-react-redux-and-firestore-from-scratch/) course.

## 🏠 [Homepage](https://bitbucket.org/ahristov/revents/)

## Install

```sh
yarn install
```

, or just `yarn`.

## Usage

```sh
yarn start
```

## Run tests

```sh
yarn test
```

## Author

👤 **Atanas Hristov**

## Course notes

Adding dependencies:
c

```bash
yarn add cuid @types/cuid date-fns firebase google-map-react @types/google-map-react moment react-cropper @types/react-cropper react-datepicker @types/react-datepicker react-dropzone @types/react-dropzone react-infinite-scroller @types/react-infinite-scroller react-lazyload @types/react-lazyload react-places-autocomplete @types/react-places-autocomplete react-redux @types/react-redux  react-redux-firebase react-redux-toastr @types/react-redux-toastr react-router-dom @types/react-router-dom redux redux-auth-wrapper @types/redux-auth-wrapper redux-firestore redux-form @types/redux-form redux-thunk revalidate @types/revalidate semantic-ui-react
```

### Links

[Create React App with TypeScript](https://facebook.github.io/create-react-app/docs/adding-typescript)  
[Semantic UI](https://semantic-ui.com/)  
[Semantic UI React](https://react.semantic-ui.com/)  
[React Redux Firestone Course Snippets](https://marketplace.visualstudio.com/items?itemName=TCL.reactreduxcoursesnippets)
[Webpack TypeScript module.hot does not exist](https://stackoverflow.com/questions/40568176/webpack-typescript-module-hot-does-not-exist)
