import React, { Component } from 'react'

/*
const App: React.FC = () => {
  return (
    <div className="App">
      <h1>Re-vents</h1>
    </div>
  )
}
*/

class App extends Component {
  render() {
    return (
      <div className="App">
        <h1>Re-vents</h1>
      </div>
    )
  }
}

export default App
